defmodule Phx16.Repo do
  use Ecto.Repo,
    otp_app: :phx16,
    adapter: Ecto.Adapters.Postgres
end
